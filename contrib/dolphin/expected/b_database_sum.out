-- test Case 1: Basic SUM() test with numeric values
-- Create a table with a numeric column
CREATE TABLE test_sum_numeric (
    id INT AUTO_INCREMENT PRIMARY KEY,
    value INT
);
NOTICE:  CREATE TABLE will create implicit sequence "test_sum_numeric_id_seq" for serial column "test_sum_numeric.id"
NOTICE:  CREATE TABLE / PRIMARY KEY will create implicit index "test_sum_numeric_pkey" for table "test_sum_numeric"
-- Insert test data
INSERT INTO test_sum_numeric (value) VALUES (1), (2), (3), (4), (5);
-- Calculate the sum of all values in the column
SELECT SUM(value) AS total_sum FROM test_sum_numeric;
 total_sum 
-----------
        15
(1 row)

-- test Case 2: SUM() with DISTINCT for numeric values
-- Calculate the sum of distinct values in the column
SELECT SUM(DISTINCT value) AS distinct_sum FROM test_sum_numeric;
 distinct_sum 
--------------
           15
(1 row)

-- test Case 3: Empty table test for numeric values
-- Create an empty table with a numeric column
CREATE TABLE test_sum_numeric_empty (
    id INT AUTO_INCREMENT PRIMARY KEY,
    value INT
);
NOTICE:  CREATE TABLE will create implicit sequence "test_sum_numeric_empty_id_seq" for serial column "test_sum_numeric_empty.id"
NOTICE:  CREATE TABLE / PRIMARY KEY will create implicit index "test_sum_numeric_empty_pkey" for table "test_sum_numeric_empty"
-- Calculate the sum of values in the empty table
SELECT SUM(value) AS total_sum FROM test_sum_numeric_empty;
 total_sum 
-----------
          
(1 row)

-- test Case 4: Single - value test for numeric values
-- Create a table with a single numeric value
CREATE TABLE test_sum_numeric_single (
    id INT AUTO_INCREMENT PRIMARY KEY,
    value INT
);
NOTICE:  CREATE TABLE will create implicit sequence "test_sum_numeric_single_id_seq" for serial column "test_sum_numeric_single.id"
NOTICE:  CREATE TABLE / PRIMARY KEY will create implicit index "test_sum_numeric_single_pkey" for table "test_sum_numeric_single"
-- Insert a single value
INSERT INTO test_sum_numeric_single (value) VALUES (10);
-- Calculate the sum of the single value
SELECT SUM(value) AS total_sum FROM test_sum_numeric_single;
 total_sum 
-----------
        10
(1 row)

-- test Case 5: test with string values that can be converted to numbers
-- Create a table with a string column
CREATE TABLE test_sum_string_convertible (
    id INT AUTO_INCREMENT PRIMARY KEY,
    value VARCHAR(20)
);
NOTICE:  CREATE TABLE will create implicit sequence "test_sum_string_convertible_id_seq" for serial column "test_sum_string_convertible.id"
NOTICE:  CREATE TABLE / PRIMARY KEY will create implicit index "test_sum_string_convertible_pkey" for table "test_sum_string_convertible"
-- Insert test data
INSERT INTO test_sum_string_convertible (value) VALUES ('1'), ('2'), ('3');
-- Calculate the sum of string values that can be converted to numbers
SELECT SUM(value) AS total_sum FROM test_sum_string_convertible;
 total_sum 
-----------
         6
(1 row)

-- Calculate the sum of distinct string values that can be converted to numbers
SELECT SUM(DISTINCT value) AS distinct_sum FROM test_sum_string_convertible;
 distinct_sum 
--------------
            6
(1 row)

-- test Case 6: test with non - convertible string values
-- Create a table with a string column
CREATE TABLE test_sum_string_non_convertible (
    id INT AUTO_INCREMENT PRIMARY KEY,
    value VARCHAR(20)
);
NOTICE:  CREATE TABLE will create implicit sequence "test_sum_string_non_convertible_id_seq" for serial column "test_sum_string_non_convertible.id"
NOTICE:  CREATE TABLE / PRIMARY KEY will create implicit index "test_sum_string_non_convertible_pkey" for table "test_sum_string_non_convertible"
-- Insert test data
INSERT INTO test_sum_string_non_convertible (value) VALUES ('abc'), ('def'), ('ghi');
-- Calculate the sum of non - convertible string values
SELECT SUM(value) AS total_sum FROM test_sum_string_non_convertible;
WARNING:  invalid input syntax for type double precision: "abc"
WARNING:  invalid input syntax for type double precision: "def"
WARNING:  invalid input syntax for type double precision: "ghi"
 total_sum 
-----------
         0
(1 row)

-- test Case 7: Mixed numeric and non - numeric values
-- Create a table with a mixed column
CREATE TABLE test_sum_mixed (
    id INT AUTO_INCREMENT PRIMARY KEY,
    value VARCHAR(20)
);
NOTICE:  CREATE TABLE will create implicit sequence "test_sum_mixed_id_seq" for serial column "test_sum_mixed.id"
NOTICE:  CREATE TABLE / PRIMARY KEY will create implicit index "test_sum_mixed_pkey" for table "test_sum_mixed"
-- Insert test data
INSERT INTO test_sum_mixed (value) VALUES ('1'), ('2'), ('abc');
-- Calculate the sum of mixed values
SELECT SUM(value) AS total_sum FROM test_sum_mixed;
WARNING:  invalid input syntax for type double precision: "abc"
 total_sum 
-----------
         3
(1 row)

-- Calculate the sum of distinct mixed values
SELECT SUM(DISTINCT value) AS distinct_sum FROM test_sum_mixed;
WARNING:  invalid input syntax for type double precision: "abc"
 distinct_sum 
--------------
            3
(1 row)

DROP TABLE IF EXISTS test_sum_numeric;
DROP TABLE IF EXISTS test_sum_numeric_empty;
DROP TABLE IF EXISTS test_sum_numeric_single;
DROP TABLE IF EXISTS test_sum_string_convertible;
DROP TABLE IF EXISTS test_sum_string_non_convertible;
DROP TABLE IF EXISTS test_sum_mixed;
